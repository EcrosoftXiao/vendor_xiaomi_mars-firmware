# android_vendor_xiaomi_mars-firmware

Firmware images for Xiaomi 11 Pro (mars), to include in custom ROM builds.

**Current version**: fw_star_miui_STAR_V13.0.13.0.SKACNXM_55b7e41faf_12.0.zip

### How to use?

1. Clone this repo to `vendor/xiaomi/mars-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/mars-firmware/BoardConfigVendor.mk
```
